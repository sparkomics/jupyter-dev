import os
import sys
import oci
import filecmp
from oci.object_storage import UploadManager
from oci.object_storage.transfer.constants import MEBIBYTE

def do_refresh_common():
	refresh_flg = os.environ.get('JPY_REFRESH_COMMONS')
	if refresh_flg != 'True':
		print("Refreshing not set. Exiting")
		return
	print("Refreshing commons")
	# Set up config
	config = oci.config.from_file("~/.oci/config", "DEFAULT")
	
	from oci.object_storage.models import CreateBucketDetails
	object_storage = oci.object_storage.ObjectStorageClient(config)
	namespace = object_storage.get_namespace().data
	bucket_name = os.environ.get('JPY_USER_BUCKET')
	prefix = "common/"
	kwargs = {"prefix":prefix}
	response = oci.pagination.list_call_get_all_results(object_storage.list_objects, namespace, bucket_name, **kwargs)
	
	if len(response.data.objects) > 0:
		os.system("rm -rf ~/common")
		os.system("mkdir ~/common")
	
	print("Loading commons")
	
	for obj in response.data.objects:
		if obj.name != prefix:
			print('{}'.format(obj.name))
			get_obj = object_storage.get_object(namespace, bucket_name, obj.name)
			with open(obj.name, 'wb') as f:
				for chunk in get_obj.data.raw.stream(1024 * 1024, decode_content=False):
					f.write(chunk)
	
	print("Loaded commons")

if __name__ == '__main__':
	do_refresh_common()

