#!/bin/bash
set -e

# install extension configurator
pip install jupyter_nbextensions_configurator
jupyter nbextensions_configurator enable --sys-prefix

# install bunch of random extensions
pip install jupyter_contrib_nbextensions
jupyter contrib nbextension install --sys-prefix

# Enable table-of-contents extention
jupyter nbextension enable toc2/main --sys-prefix

# Enable execution time extention
jupyter nbextension enable execute_time/ExecuteTime --sys-prefix

# Enable beautiful tables
jupyter nbextension enable table_beautifier/main --sys-prefix

# Enable gist-ing of notebooks
#jupyter nbextension enable gist_it/main --sys-prefix

jupyter nbextension enable addbefore/main



# Add RISE slideshow extension
pip install RISE && \
    jupyter-nbextension install rise --py --sys-prefix
#jupyter-nbextension enable rise --py --sys-prefix

# add ipython-sql
pip install ipython-sql

# Add dashboarding install using pip from pypi and then activate the extension
pip install jupyter_dashboards
#jupyter dashboards quick-setup --sys-prefix

pip install lxml
pip install jupyterthemes

pip install nbgitpuller && \
       jupyter serverextension enable --py nbgitpuller --sys-prefix


