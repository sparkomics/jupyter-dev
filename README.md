# Jupyter Notebook Docker Image

# Installation  

## Install necessary dependencies
- sudo yum install -y docker
- sudo yum install -y git
- Install OCI CLI: https://docs.cloud.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm

## Download Jupyter Notebook Docker with Spark

- cd
- git clone https://gitlab.com/sparkomics/jupyter-dev.git
- cd jupyter-dev

## Build Docker image

- cd docker
- docker build -t jupyter-dev .
- cd ..

## Add a firewall rule
- sudo firewall-cmd --zone=public --add-port=443/tcp --permanent
- sudo firewall-cmd --reload

## Add a Ingress rule in your VCN to allow incoming HTTPS traffic (port 443)


## Copy docker-notebook.env file to your home directory (ex., /home/opc/docker-notebook.env) and supply your environment parameters
If you would like to store notebooks in OCI bucket:
- JPY_USE_OCI_BUCKET=True (set it to False for storing notebooks locally)
- JPY_REFRESH_COMMONS=True (if you would like to store Spark connection info in a special shared notebook in OCI bucket)
Go to OCI console, create a bucket for your notebooks.    
Then, go to your user and create a Customer Secret Key. The key will be used by the Notebook server to access the bucket on your behalf.   
- JPY_USER_BUCKET - name of OCI bucket for your notebooks
- JPY_USER_BUCKET_ACCESS_KEY_ID - a customer secrect key ID (Access Key)
- JPY_USER_BUCKET_SECRET_ACCESS_KEY - the key obtained when creating a customer secrect key
- JPY_USER_BUCKET_ENDPOINT_URL - the URL has the following format:    
https://<YOUR NAMESPACE NAME>.compat.objectstorage.<REGION>.oraclecloud.com    
For example, https://mycompany.compat.objectstorage.us-ashburn-1.oraclecloud.com    

- JPY_PSWD - optionally, notebook server password. If not supplied, a standard Jupyuter token authentication will be used.   


# Start Jupyter Notebook
- ./start_docker_notebook.sh

The server will be available at https://<your host>
