#!/bin/bash
# install jupyter drive. enable with 'python -m jupyterdrive --user --mixed'
pip install git+https://github.com/jupyter/jupyter-drive.git
#python3 -m jupyterdrive --user --mixed

# install nice jupyter theme.  enable with:
pushd /tmp
git clone https://github.com/red8012/Adrastea.git
pushd Adrastea
jupyter nbextension install AdrasteaNotebookExtension --sys-prefix
popd
rm -rf Adrastea
popd
jupyter nbextension enable AdrasteaNotebookExtension/main --sys-prefix                         

